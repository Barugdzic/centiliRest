/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.controller;

import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import nenad.centilitest.client.DocumentItemRestClient;
import nenad.centilitest.client.DocumentRestClient;
import nenad.centilitest.common.LoggableBean;
import nenad.centilitest.dto.DocumentDTO;
import nenad.centilitest.dto.DocumentItemDTO;
import nenad.centilitest.dto.DocumentItemListDTO;
import nenad.centilitest.dto.DocumentListDTO;

/**
 * Bean for managing JSF page index
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
@ApplicationScoped
@ManagedBean(name = "docBean")
public class DocumentsAndItemsController extends LoggableBean {

    private DocumentListDTO documentList;
    private boolean docForm;
    private boolean itemForm;
    private DocumentDTO selectedDoc;
    private DocumentItemDTO selectedItem;
    private DocumentItemListDTO allItems;

    /**
     * Getting list of all documents for data table with id=docTable
     *
     * @return DocumentListDTO entity with a list of documents
     */
    public DocumentListDTO getDocumentList() {
        if (documentList == null || documentList.getDocuments() == null) {
            try {
                DocumentRestClient client = new DocumentRestClient();
                documentList = client.findAll(DocumentListDTO.class);
                client.close();
                if (documentList.isFailed()) {
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, LOG_MESSAGE + "getting all documents!", null);
                    FacesContext.getCurrentInstance().addMessage("Error", message);
                    printLog(true, "getDocumentList", "");
                }
            } catch (Exception e) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
                FacesContext.getCurrentInstance().addMessage("Error", message);
                Logger.getLogger(DocumentsAndItemsController.class.getName()).log(Level.SEVERE, " getDocumentList: ", e);
                printLog(true, "getDocumentList", e.getMessage());
            }
        }
        return documentList;
    }

    public void setDocumentList(DocumentListDTO documentList) {
        this.documentList = documentList;
    }

    /**
     * Removing selected item in table with id=itemTable from table
     * document_items in database, then from all lists where that object was
     * putted
     *
     */
    public void removeItemClick() {
        try {
            if (getSelectedItem() == null) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "You must select item", null);
                FacesContext.getCurrentInstance().addMessage("Info", message);
            } else {
                if (getSelectedDoc() == null || !Objects.equals(getSelectedDoc().getId(), getSelectedItem().getId_document())) {
                    DocumentRestClient client = new DocumentRestClient();
                    DocumentDTO temp = client.findDocument(DocumentDTO.class, String.valueOf(getSelectedItem().getId_document()));
                    if (temp.isFailed()) {
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, LOG_MESSAGE + "finding document for item ", null);
                        FacesContext.getCurrentInstance().addMessage("Error", message);
                        printLog(true, "removeItemClick", "id_document: " + getSelectedItem().getId_document());
                        client.close();
                        return;
                    }
                    setSelectedDoc(temp);
                }
                DocumentItemRestClient client = new DocumentItemRestClient();
                int status = client.remove(String.valueOf(getSelectedItem().getId()));
                client.close();
                if (status < 400) {
                    int tempIdx = 0;
                    for (int i = 0; i < getSelectedDoc().getItems().size(); i++) {
                        if (Objects.equals(getSelectedDoc().getItems().get(i).getId(), getSelectedItem().getId())) {
                            tempIdx = i;
                            break;
                        }
                    }
                    getSelectedDoc().getItems().remove(tempIdx);
                    for (int i = 0; i < getAllItems().getItems().size(); i++) {
                        if (Objects.equals(getAllItems().getItems().get(i).getId(), getSelectedItem().getId())) {
                            tempIdx = i;
                            break;
                        }
                    }
                    getAllItems().getItems().remove(tempIdx);
                    printLog(false, "removeItemClick", null);
                } else {
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, LOG_MESSAGE + "deleting items: " + status, null);
                    FacesContext.getCurrentInstance().addMessage("Error", message);
                    printLog(true, "removeItemClick", "status = " + status);
                }
            }
        } catch (Exception e) {
            setAllItems(null);
            setSelectedDoc(null);
            setSelectedItem(null);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
            FacesContext.getCurrentInstance().addMessage("Error", message);
            Logger.getLogger(DocumentsAndItemsController.class.getName()).log(Level.SEVERE, " removeItemClick: ", e);
            printLog(true, "removeItemClick", e.getMessage());
        }
    }

    /**
     * Removing selected document in table id=docTable from table documents in
     * database, then from all lists where that object was putted
     *
     */
    public void removeDocClick() {
        try {
            if (getSelectedDoc() == null) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "You must select document", null);
                FacesContext.getCurrentInstance().addMessage("Info", message);
            } else {
                DocumentRestClient client = new DocumentRestClient();
                int status = client.remove(String.valueOf(getSelectedDoc().getId()));
                client.close();
                if (status < 400) {
                    getDocumentList().getDocuments().remove(getSelectedDoc());
                    printLog(false, "removeDocClick", null);
                    setSelectedDoc(null);
                    setSelectedItem(null);
                    setAllItems(null);
                } else {
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, LOG_MESSAGE + "deleting documentwith with error: " + status, null);
                    FacesContext.getCurrentInstance().addMessage("Error", message);
                    printLog(true, "removeDocClick", "status = " + status);
                }
            }
        } catch (Exception e) {
            setDocumentList(null);
            setAllItems(null);
            setSelectedDoc(null);
            setSelectedItem(null);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
            FacesContext.getCurrentInstance().addMessage("Error", message);
            Logger.getLogger(DocumentsAndItemsController.class.getName()).log(Level.SEVERE, " removeDocClick: ", e);
            printLog(true, "removeDocClick", e.getMessage());
        }
    }

    public boolean getDocForm() {
        return docForm;
    }

    public void setDocForm(boolean docForm) {
        this.docForm = docForm;
    }

    public boolean getItemForm() {
        return itemForm;
    }

    public void setItemForm(boolean itemForm) {
        this.itemForm = itemForm;
    }

    /**
     * Methods to check whether selected document has or has not items
     *
     * @return true if selected document has item, false if has not
     */
    public boolean getHasItems() {
        return getSelectedDoc() != null && getSelectedDoc().getItems() != null && !getSelectedDoc().getItems().isEmpty();
    }

    /**
     * Opening list of items if selected document has items
     *
     */
    public void onRowSelect() {
        try {
            if (getSelectedDoc() == null) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "You must select document", null);
                FacesContext.getCurrentInstance().addMessage("Info", message);
            } else {
                if (getSelectedDoc().getItems() == null || getSelectedDoc().getItems().isEmpty()) {
                    DocumentItemRestClient client = new DocumentItemRestClient();
                    DocumentItemListDTO list = client.findAllDocumentItemsByDocument(DocumentItemListDTO.class, String.valueOf(getSelectedDoc().getId()));
                    client.close();
                    if (list.isFailed()) {
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, LOG_MESSAGE + "getting items for document", null);
                        FacesContext.getCurrentInstance().addMessage("Error", message);
                        Logger.getLogger(DocumentsAndItemsController.class.getName()).log(Level.SEVERE, " An error has been occured while getting items for document");
                        printLog(true, "onRowSelect", null);
                        return;
                    }
                    if (list.getItems() == null || list.getItems().isEmpty()) {
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "No items for selected document", null);
                        FacesContext.getCurrentInstance().addMessage("Info", message);
                        return;
                    }
                    getSelectedDoc().setItems(list.getItems());
                }
                printLog(false, "onRowSelect", "id_document" + getSelectedDoc().getId());
            }
        } catch (Exception e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
            FacesContext.getCurrentInstance().addMessage("Error", message);
            Logger.getLogger(DocumentsAndItemsController.class.getName()).log(Level.SEVERE, " onRowSelect: ", e);
            printLog(true, "onRowSelect", e.getMessage());
        }
    }

    public DocumentDTO getSelectedDoc() {
        return selectedDoc;
    }

    public void setSelectedDoc(DocumentDTO selectedDoc) {
        this.selectedDoc = selectedDoc;
    }

    public DocumentItemDTO getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(DocumentItemDTO selectedItem) {
        this.selectedItem = selectedItem;
    }

    /**
     * Getting list of all items for data table with id=allItemTable
     *
     * @return DocumentItemListDTO entity with a list of documents
     */
    public DocumentItemListDTO getAllItems() {
        if (allItems == null || allItems.getItems() == null) {
            try {
                DocumentItemRestClient client = new DocumentItemRestClient();
                allItems = client.findAll(DocumentItemListDTO.class);
                client.close();
                if (allItems.isFailed()) {
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, LOG_MESSAGE + "getting all items!", null);
                    FacesContext.getCurrentInstance().addMessage("Error", message);
                    printLog(true, "getAllItems", "");
                }
            } catch (Exception e) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
                FacesContext.getCurrentInstance().addMessage("Error", message);
                Logger.getLogger(DocumentsAndItemsController.class.getName()).log(Level.SEVERE, " getAllItems: ", e);
                printLog(true, "getAllItems", e.getMessage());
            }
        }
        return allItems;
    }

    public void setAllItems(DocumentItemListDTO allItems) {
        this.allItems = allItems;
    }

    /**
     * Returns a message to dialog for deleting selected document
     *
     * @return message
     */
    public String getDocDeleteMessage() {
        if (getSelectedDoc() == null) {
            return "You must select document";
        } else if (getSelectedDoc() != null && getSelectedDoc().getItems() != null && !getSelectedDoc().getItems().isEmpty()) {
            return "You have items created for document: " + selectedDoc.getName() + ". All of them will be deleted.";
        } else {
            return null;
        }
    }

    /**
     * Returns a message to dialog for deleting selected item
     *
     * @return message
     */
    public String getItemDeleteMessage() {
        if (getSelectedItem() == null) {
            return "You must select item";
        } else {
            return null;
        }
    }
}

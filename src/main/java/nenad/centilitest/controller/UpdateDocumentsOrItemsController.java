/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import nenad.centilitest.client.DocumentItemRestClient;
import nenad.centilitest.client.DocumentRestClient;
import nenad.centilitest.common.LoggableBean;
import nenad.centilitest.dto.DocumentDTO;
import nenad.centilitest.dto.DocumentItemDTO;

/**
 * Bean for managing JSF page pageForUpdate
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
@ApplicationScoped
@ManagedBean(name = "addBean")
public class UpdateDocumentsOrItemsController extends LoggableBean {

    private DocumentDTO docDto;
    private DocumentItemDTO itemDto;
    private String messages;
    @ManagedProperty(value = "#{docBean}")
    DocumentsAndItemsController docBean;
    private static final String updateDocPath = "/pageForUpdate.centili";

    public DocumentDTO getDocDto() {
        if (docDto == null) {
            docDto = new DocumentDTO();
        }
        return docDto;
    }

    public void setDocDto(DocumentDTO docDto) {
        this.docDto = docDto;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    /**
     * Method for updating selected or creating new document. When document is
     * created, or updated, data table with id=docTable will be also updated
     * with right values (documentList of DocumentsAndItemsController is
     * changed)
     *
     */
    public void updateDocumentButton() {
        try {
            if (docDto.getName().trim().equals("") || docDto.getCode().trim().equals("")) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Name and code cannot be null and cannot be blank", null);
                FacesContext.getCurrentInstance().addMessage("Info", message);
                return;
            }
            List<DocumentDTO> tempList = getDocBean().getDocumentList().getDocuments();
            if (tempList == null) {
                tempList = new ArrayList<DocumentDTO>();
            }
            for (DocumentDTO d : tempList) {
                if (d.getCode().equals(getDocDto().getCode()) && !(Objects.equals(d.getId(), docDto.getId()))) {
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Document with code: " + docDto.getCode() + " already exist.", null);
                    FacesContext.getCurrentInstance().addMessage("Info", message);
                    return;
                }
            }
            DocumentRestClient client = new DocumentRestClient();
            String action = "";
            if (getDocDto().getId() == null) {
                docDto = client.create(docDto, DocumentDTO.class);
                action = "CREATE";
                if (docDto.isFailed()) {
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, LOG_MESSAGE + " inserting new document", null);
                    FacesContext.getCurrentInstance().addMessage("Error", message);
                    printLog(true, "updateDocumentButton", null);
                    setAllToNull();
                    client.close();
                    return;
                }
            } else {
                docDto = client.edit(docDto, DocumentDTO.class);
                if (docDto.isFailed()) {
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, LOG_MESSAGE + " editing document", null);
                    FacesContext.getCurrentInstance().addMessage("Error", message);
                    printLog(true, "updateDocumentButton", "id_document = " + docDto.getId());
                    client.close();
                    setAllToNull();
                    return;
                }
                action = "EDIT";
                int tempIdx = 0;
                for (int i = 0; i < tempList.size(); i++) {
                    if (Objects.equals(tempList.get(i).getId(), docDto.getId())) {
                        tempIdx = i;
                        break;
                    }
                }
                tempList.remove(tempIdx);
            }
            client.close();
            tempList.add(docDto);
            getDocBean().getDocumentList().setDocuments(tempList);
            getDocBean().setSelectedDoc(docDto);
            printLog(false, "updateDocumentButton" + action, "id_document = " + docDto.getId() + ", date: " + docDto.getDate());
            getDocBean().setSelectedDoc(docDto);
            cancel();
        } catch (Exception e) {
            setAllToNull();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
            FacesContext.getCurrentInstance().addMessage("Error", message);
            Logger.getLogger(UpdateDocumentsOrItemsController.class.getName()).log(Level.SEVERE, " updateDocumentButton: ", e);
            printLog(true, "updateDocumentButton", e.getMessage());
        }
    }

    /**
     * Method for updating selected or creating new item. When item is created
     * or updated, data table with id=itemTable and data table with with
     * id=allItemsTable will be also updated with right values (list of items in
     * selectedItem and allItems property in DocumentsAndItemsController are
     * changed)
     *
     */
    public void updateItemButton() {
        try {
            if (getItemDto().getName().trim().equals("")) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Name cannot be null and cannot be blank", null);
                FacesContext.getCurrentInstance().addMessage("Info", message);
                return;
            }
            if (getItemDto().getPrice() <= 0) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Price must be bigger than zero", null);
                FacesContext.getCurrentInstance().addMessage("Error", message);
            } else {
                List<DocumentItemDTO> tempList = getDocBean().getSelectedDoc().getItems();
                if (tempList == null) {
                    tempList = new ArrayList<DocumentItemDTO>();
                }
                for (DocumentItemDTO d : tempList) {
                    if (d.getName().equals(itemDto.getName()) && (Objects.equals(d.getId_document(), itemDto.getId_document())) && !(Objects.equals(d.getId(), itemDto.getId()))) {
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item with name: " + itemDto.getName() + " for document: " + getDocBean().getSelectedDoc().getName() + " already exist.", null);
                        FacesContext.getCurrentInstance().addMessage("Info", message);
                        return;
                    }
                }
                List<DocumentItemDTO> tempAllList = docBean.getAllItems().getItems();
                DocumentItemRestClient client = new DocumentItemRestClient();
                String action = "";
                if (itemDto.getId() == null) {
                    itemDto = client.create(itemDto, DocumentItemDTO.class);
                    if (itemDto.isFailed()) {
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, LOG_MESSAGE + "inserting new item with error ", null);
                        FacesContext.getCurrentInstance().addMessage("Error", message);
                        printLog(true, "updateItemButton", null);
                        client.close();
                        setAllToNull();
                        return;
                    } else {
                        if (tempAllList != null && ((tempAllList.size() == 1 && !Objects.equals(tempAllList.get(0).getId(), itemDto.getId())) || tempAllList.size() > 1)) {
                            tempAllList.add(itemDto);
                        }
                        action = "CREATE";
                    }
                } else {
                    itemDto = client.edit(itemDto, DocumentItemDTO.class);
                    if (itemDto.isFailed()) {
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, LOG_MESSAGE + "editing item ", null);
                        FacesContext.getCurrentInstance().addMessage("Error", message);
                        printLog(true, "updateItemButton", "id_item= " + itemDto.getId());
                        client.close();
                        setAllToNull();
                        return;
                    }
                    int tempIdx = 0;
                    for (int i = 0; i < tempList.size(); i++) {
                        if (Objects.equals(tempList.get(i).getId(), itemDto.getId())) {
                            tempIdx = i;
                            break;
                        }
                    }
                    tempList.remove(tempIdx);
                    tempIdx = 0;
                    for (int i = 0; i < tempAllList.size(); i++) {
                        if (Objects.equals(tempAllList.get(i).getId(), itemDto.getId())) {
                            tempIdx = i;
                            break;
                        }
                    }
                    tempAllList.remove(tempIdx);
                    action = "EDIT";
                    tempAllList.add(itemDto);
                }
                tempList.add(itemDto);
                getDocBean().getSelectedDoc().setItems(tempList);
                getDocBean().setSelectedItem(itemDto);
                client.close();
                printLog(false, "updateItemButton" + action, "id_item = " + itemDto.getId());
                cancel();

            }
        } catch (Exception e) {
            setAllToNull();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
            FacesContext.getCurrentInstance().addMessage("Error", message);
            Logger.getLogger(UpdateDocumentsOrItemsController.class.getName()).log(Level.SEVERE, " updateItemButton: ", e);
            printLog(true, "updateItemButton", e.getMessage());
        }
    }

    public void cancel() {
        try {
            messages = null;
            docDto = null;
            itemDto = null;
            getDocBean().setDocForm(false);
            getDocBean().setItemForm(false);
            ExternalContext eCtx = FacesContext.getCurrentInstance().getExternalContext();
            HttpServletRequest request = (HttpServletRequest) eCtx.getRequest();
            eCtx.redirect(request.getContextPath());
        } catch (Exception e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
            FacesContext.getCurrentInstance().addMessage("Error", message);
            Logger.getLogger(UpdateDocumentsOrItemsController.class.getName()).log(Level.SEVERE, " cancel: ", e);
            printLog(true, "cancel", e.getMessage());
        }
    }

    public DocumentItemDTO getItemDto() {
        return itemDto;
    }

    public void setItemDto(DocumentItemDTO itemDto) {
        this.itemDto = itemDto;
    }

    /**
     * Open page pageForUpdate for changing selected item. When item is changed,
     * values in lists in allItems and selectedDoc are updated
     *
     */
    public void editItemClick() {
        try {
            if (getDocBean().getSelectedItem() == null) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "You must select item", null);
                FacesContext.getCurrentInstance().addMessage("Info", message);
            } else {
                if (getDocBean().getSelectedDoc() == null || !Objects.equals(getDocBean().getSelectedDoc().getId(), getDocBean().getSelectedItem().getId_document())) {
                    DocumentRestClient client = new DocumentRestClient();
                    DocumentDTO temp = client.findDocument(DocumentDTO.class, String.valueOf(getDocBean().getSelectedItem().getId_document()));
                    if (temp.isFailed()) {
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, LOG_MESSAGE + "finding document for item ", null);
                        FacesContext.getCurrentInstance().addMessage("Error", message);
                        printLog(true, "editItemClick", "id_document: " + getDocBean().getSelectedItem().getId_document());
                        client.close();
                        return;
                    }
                    getDocBean().setSelectedDoc(temp);
                    client.close();
                }
                DocumentItemDTO temp = getDocBean().getSelectedItem();
                itemDto = new DocumentItemDTO();
                itemDto.setDocCode(temp.getDocCode());
                itemDto.setId(temp.getId());
                itemDto.setId_document(temp.getId_document());
                itemDto.setName(temp.getName());
                itemDto.setPrice(temp.getPrice());
                getDocBean().setItemForm(true);
                getDocBean().setDocForm(false);
                updateClick(updateDocPath);
            }
        } catch (Exception e) {
            getDocBean().setSelectedItem(null);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
            FacesContext.getCurrentInstance().addMessage("Error", message);
            Logger.getLogger(UpdateDocumentsOrItemsController.class.getName()).log(Level.SEVERE, " editItemClick: ", e);
            printLog(true, "editItemClick", e.getMessage());
        }
    }

    /**
     * Open page pageForUpdate for changing selected document. When document is
     * changed, values in lists in documentList are updated
     *
     */
    public void editDocClick() {
        try {
            if (getDocBean().getSelectedDoc() == null) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "You must select document", null);
                FacesContext.getCurrentInstance().addMessage("Info", message);
            } else {
                DocumentDTO temp = getDocBean().getSelectedDoc();
                setDocDto(new DocumentDTO());
                docDto.setId(temp.getId());
                docDto.setCode(temp.getCode());
                docDto.setName(temp.getName());
                docDto.setDate(temp.getDate());
                docDto.setItems(temp.getItems());
                getDocBean().setDocForm(true);
                getDocBean().setItemForm(false);
                updateClick(updateDocPath);
            }
        } catch (Exception e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
            FacesContext.getCurrentInstance().addMessage("Error", message);
            Logger.getLogger(UpdateDocumentsOrItemsController.class.getName()).log(Level.SEVERE, " editDocClick: ", e);
            printLog(true, "editDocClick", e.getMessage());
        }
    }

    /**
     * Open page pageForUpdate for creating new document
     *
     */
    public void addDocumentClick() {
        try {
            getDocBean().setDocForm(true);
            getDocBean().setItemForm(false);
            updateClick(updateDocPath);
        } catch (Exception e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
            FacesContext.getCurrentInstance().addMessage("Error", message);
            Logger.getLogger(UpdateDocumentsOrItemsController.class.getName()).log(Level.SEVERE, " addDocumentClick: ", e);
            printLog(true, "addDocumentClick", e.getMessage());
        }
    }

    /**
     * Open page pageForUpdate for creating new item
     *
     */
    public void addItemClick() {
        try {
            if (getDocBean().getSelectedDoc() == null) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "You must select document", null);
                FacesContext.getCurrentInstance().addMessage("Info", message);
            } else {
                if (itemDto == null) {
                    setItemDto(new DocumentItemDTO());
                    itemDto.setId_document(getDocBean().getSelectedDoc().getId());
                }
                getDocBean().setItemForm(true);
                getDocBean().setDocForm(false);
                updateClick(updateDocPath);
            }
        } catch (Exception e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
            FacesContext.getCurrentInstance().addMessage("Error", message);
            Logger.getLogger(UpdateDocumentsOrItemsController.class.getName()).log(Level.SEVERE, " addItemClick: ", e);
            printLog(true, "addItemClick", e.getMessage());
        }
    }

    public DocumentsAndItemsController getDocBean() {
        return docBean;
    }

    public void setDocBean(DocumentsAndItemsController docBean) {
        this.docBean = docBean;
    }

    private void setAllToNull() {
        getDocBean().setDocumentList(null);
        getDocBean().setAllItems(null);
        getDocBean().setSelectedDoc(null);
        getDocBean().setSelectedItem(null);
    }
}

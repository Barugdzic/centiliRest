/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.sessions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import nenad.centilitest.entity.DocumentItem;

/**
 * Class for managing items entity with database
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
@Stateless
public class DocumentItemFacade extends AbstractFacade<DocumentItem> {

    @PersistenceContext(unitName = "CentiliPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DocumentItemFacade() {
        super(DocumentItem.class);
    }

    /**
     * Method for finding all items of selected document
     *
     * @param id_document id of selected document
     * @return list of items
     */
    public List<DocumentItem> findAllOfDocument(Long id_document) {
        try {
            TypedQuery<DocumentItem> query = getEntityManager().createNamedQuery("DocumentItem.findAllByDocumentId", DocumentItem.class);
            query.setParameter("id_document", id_document);
            return query.getResultList();
        } catch (Exception e) {
            Logger.getLogger(DocumentItemFacade.class.getName()).log(Level.SEVERE, " findAllOfDocument", e);
            throw e;
        }
    }

    /**
     * Method for finding last created Item entity
     *
     * @return last created item
     */
    public DocumentItem findLastInserted() {
        try {
            TypedQuery<DocumentItem> query = getEntityManager().createNamedQuery("DocumentItem.findLastInserted", DocumentItem.class);
            return query.getSingleResult();
        } catch (Exception e) {
            Logger.getLogger(DocumentItemFacade.class.getName()).log(Level.SEVERE, " findLastInserted", e);
            throw e;
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.sessions;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import nenad.centilitest.entity.Document;

/**
 * Class for managing documents entity with database
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
@Stateless
public class DocumentFacade extends AbstractFacade<Document> {

    @PersistenceContext(unitName = "CentiliPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DocumentFacade() {
        super(Document.class);
    }

    /**
     * Method for finding last created Document entity
     *
     * @return last created document
     */
    public Document findLastInserted() {
        try {
            TypedQuery<Document> query = getEntityManager().createNamedQuery("Document.findLastInserted", Document.class);
            return query.getSingleResult();
        } catch (Exception e) {
            Logger.getLogger(DocumentFacade.class.getName()).log(Level.SEVERE, " findLastInserted", e);
            throw e;
        }
    }

    /**
     * Method for deleting all items for selected document
     *
     * @param id_document id of selected document
     */
    public void deleteItemsByIdDocument(long id_document) {
        try {
            Query query = getEntityManager().createNamedQuery("DocumentItem.deleteByDocument");
            query.setParameter("id_document", id_document);
            query.executeUpdate();
        } catch (Exception e) {
            Logger.getLogger(DocumentFacade.class.getName()).log(Level.SEVERE, " deleteItemsByIdDocument", e);
            throw e;
        }
    }
}

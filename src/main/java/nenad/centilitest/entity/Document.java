/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Entity representing table documents in database
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
@Entity
@Table(name = "documents")
@NamedQueries({
    @NamedQuery(name = "Document.findLastInserted", query = "SELECT d FROM Document d WHERE d.id = (SELECT MAX(da.id) FROM Document da)")
})
public class Document implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_document")
    private Long id;
    @Column(name = "code_document", nullable = false, length = 100, unique = true)
    private String code;
    @Column(name = "date_document", nullable = false)
    private Date date;
    @Column(name = "name_document", nullable = false, length = 100)
    private String name;
    @OneToMany(mappedBy = "document", fetch = FetchType.EAGER)
    private List<DocumentItem> items;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public List<DocumentItem> getItems() {
        return items;
    }

    public void setItems(List<DocumentItem> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Document{" + "id=" + id + ", code=" + code + ", date=" + date + ", name=" + name + '}';
    }

}

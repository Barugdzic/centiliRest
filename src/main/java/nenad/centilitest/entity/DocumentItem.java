/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity representing table document_items in database
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
@Entity
@XmlRootElement
@Table(name = "document_items")
@NamedQueries({
    @NamedQuery(name = "DocumentItem.findAllByDocumentId", query = "SELECT di FROM DocumentItem di WHERE di.document.id = :id_document ORDER BY di.name ASC"),
    @NamedQuery(name = "DocumentItem.findLastInserted", query = "SELECT di FROM DocumentItem di WHERE di.id = (SELECT MAX(da.id) FROM DocumentItem da)"),
    @NamedQuery(name = "DocumentItem.deleteByDocument", query = "DELETE FROM DocumentItem di WHERE di.document.id = :id_document")
})
public class DocumentItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_item")
    private Long id;
    @Column(name = "name_item", nullable = false)
    private String name;
    @Column(name = "price_item", nullable = false)
    private double price;
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_document", nullable = false)
    private Document document;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    @Override
    public String toString() {
        return "DocumentItem{" + "id=" + id + ", name=" + name + ", price=" + price + document.toString() + '}';
    }

}

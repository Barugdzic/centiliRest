/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.dto;

/**
 * DataTransferObject with default fields
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
public class BaseDTO {

    protected boolean failed;

    public boolean isFailed() {
        return failed;
    }

    public void setFailed(boolean failed) {
        this.failed = failed;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * DataTransferObject representing document
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
@XmlRootElement(name = "documentDTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class DocumentDTO extends BaseDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String code;
    private Date date;
    private String name;
    private List<DocumentItemDTO> items;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DocumentItemDTO> getItems() {
        return items;
    }

    public void setItems(List<DocumentItemDTO> items) {
        this.items = items;
    }

}

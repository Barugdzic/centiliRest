/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.dto;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * DataTransferObject representing list of documents
 * 
 * @author nenad <barugdzicnenad@gmail.com>
 */
@XmlRootElement(name = "documentsDTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class DocumentListDTO extends BaseDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElement(name = "documentDTO")
    private List<DocumentDTO> documents = null;

    public List<DocumentDTO> getDocuments() {
        return documents;
    }

    public void setDocuments(List<DocumentDTO> documents) {
        this.documents = documents;
    }

}

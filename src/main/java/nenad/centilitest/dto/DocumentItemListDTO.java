/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.dto;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * DataTransferObject representing list of items
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
@XmlRootElement(name = "itemsDTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class DocumentItemListDTO extends BaseDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlElement(name = "itemDTO")
    private List<DocumentItemDTO> items = null;

    public List<DocumentItemDTO> getItems() {
        return items;
    }

    public void setItems(List<DocumentItemDTO> items) {
        this.items = items;
    }

}

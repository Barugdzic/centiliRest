/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * DataTransferObject representing item
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
@XmlRootElement(name = "itemDTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class DocumentItemDTO extends BaseDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;
    private double price;
    private Long id_document;
    private String docCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getId_document() {
        return id_document;
    }

    public void setId_document(Long id_document) {
        this.id_document = id_document;
    }

    public String getDocCode() {
        return docCode;
    }

    public void setDocCode(String docCode) {
        this.docCode = docCode;
    }

}

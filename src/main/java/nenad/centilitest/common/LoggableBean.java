/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.common;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 * Helping class for logging and redirecting
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
public class LoggableBean {

    public static final String LOG_MESSAGE = "An error has been occured while ";
    /**
     * Method for printing log
     *
     * @param isError state of method execution
     * @param methodName name of method called
     * @param param list of parameters in method
     */
    public void printLog(boolean isError, String methodName, String param) {
        ExternalContext eCtx = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletRequest request = (HttpServletRequest) eCtx.getRequest();
        String contextName = request.getContextPath().substring(1);// da izbacimo prvi slash
        String status = isError ? "ROLLBACK" : "COMMIT";
        String sessionId = request.getSession().getId();
        int lenth = 0;
        Date beginAndEndTime = Calendar.getInstance().getTime();

        String requestParamsJSON = "";
        if (param != null) {
            requestParamsJSON += "{" + "\"params\"::\"" + param + "\"}";
        }
        String log = String.format("%1$s;[%2$s];<%3$s>;%4$d;%5$tY-%5$tm-%5$tdT%5$tH:%5$tM:%5$tS.%5$tL%5$tZ;%6$tY-%6$tm-%6$tdT%6$tH:%6$tM:%6$tS.%6$tL%6$tZ;%7$s;%8$s;", contextName, methodName, status, lenth, beginAndEndTime, beginAndEndTime, sessionId, requestParamsJSON);
        Logger logg = Logger.getLogger(this.getClass().getName());
        logg.info(log);
    }

    /**
     * Method for redirecting on another page
     *
     * @param path path of URL of wanted page
     * @throws java.lang.Exception
     */
    public void updateClick(String path) throws IOException {
        ExternalContext eCtx = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletRequest request = (HttpServletRequest) eCtx.getRequest();
        try {
            eCtx.redirect(request.getContextPath() + path);
        } catch (IOException ex) {
            Logger.getLogger(LoggableBean.class.getName()).log(Level.SEVERE, " Eroor on redirecting on page: " + path, ex);
            throw ex;
        }
    }

}

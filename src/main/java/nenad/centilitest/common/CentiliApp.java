/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.common;

/**
 * Class for definition of rest service
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("rest")
public class CentiliApp extends Application {

}

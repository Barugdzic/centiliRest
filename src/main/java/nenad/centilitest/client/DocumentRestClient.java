/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.client;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import nenad.centilitest.dto.DocumentDTO;
import nenad.centilitest.dto.DocumentListDTO;

/**
 * Jersey REST client generated for REST resource:DocumentFacadeREST
 * [/documents] Method names and functionalities corresponds to methods in
 * DocumentFacadeREST
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
public class DocumentRestClient {

    private final WebTarget webTarget;
    private final Client client;
    private static final String BASE_URI = "http://localhost:8080/centiliTest/rest";

    public DocumentRestClient() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("documents");
    }

    public DocumentDTO findDocument(Class<DocumentDTO> responseType, String id) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("findDoc/{0}", new Object[]{id}));
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
    }

    public DocumentDTO edit(DocumentDTO requestEntity, Class<DocumentDTO> responseType) throws ClientErrorException {
        return webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_XML).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_XML), responseType);
    }

    public DocumentDTO create(DocumentDTO requestEntity, Class<DocumentDTO> responseType) throws ClientErrorException {
        return webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_XML).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_XML), responseType);
    }

    public DocumentListDTO findAll(Class<DocumentListDTO> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("findAll");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
    }

    public int remove(String id) throws ClientErrorException {
        return webTarget.path(java.text.MessageFormat.format("/{0}", new Object[]{id})).request().delete().getStatus();
    }

    public void close() {
        client.close();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.client;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import nenad.centilitest.dto.DocumentItemDTO;
import nenad.centilitest.dto.DocumentItemListDTO;

/**
 * Jersey REST client generated for REST resource:DocumentItemFacadeREST
 * [/documentItems] Method names and functionalities corresponds to methods in
 * DocumentItemFacadeREST
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
public class DocumentItemRestClient {

    private final WebTarget webTarget;
    private final Client client;
    private static final String BASE_URI = "http://localhost:8080/centiliTest/rest";

    public DocumentItemRestClient() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("documentItems");
    }

    public DocumentItemDTO findItem(Class<DocumentItemDTO> responseType, String id) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("findItem/{0}", new Object[]{id}));
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
    }

    public DocumentItemDTO edit(DocumentItemDTO requestEntity, Class<DocumentItemDTO> responseType) throws ClientErrorException {
        return webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_XML).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_XML), responseType);
    }

    public DocumentItemDTO create(DocumentItemDTO requestEntity, Class<DocumentItemDTO> responseType) throws ClientErrorException {
        return webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_XML).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_XML), responseType);
    }

    public DocumentItemListDTO findAllDocumentItemsByDocument(Class<DocumentItemListDTO> responseType, String id_document) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("findByIdDocument/{0}", new Object[]{id_document}));
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
    }

    public int remove(String id) throws ClientErrorException {
        return webTarget.path(java.text.MessageFormat.format("/{0}", new Object[]{id})).request().delete().getStatus();
    }

    public void close() {
        client.close();
    }

    public DocumentItemListDTO findAll(Class<DocumentItemListDTO> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("findAll");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
    }

}

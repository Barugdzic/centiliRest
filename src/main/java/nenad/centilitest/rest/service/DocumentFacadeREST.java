/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.rest.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.xml.ws.http.HTTPException;
import nenad.centilitest.common.LoggableBean;
import nenad.centilitest.dto.DocumentDTO;
import nenad.centilitest.dto.DocumentItemDTO;
import nenad.centilitest.dto.DocumentListDTO;
import nenad.centilitest.entity.Document;
import nenad.centilitest.entity.DocumentItem;
import nenad.centilitest.sessions.DocumentFacade;

/**
 * REST Service for exposing Document entities via DTO
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
@Stateless(name = "DocumentFacadeREST", mappedName = "DocumentFacadeREST")
@Path("/documents")
public class DocumentFacadeREST implements Serializable {

    @EJB
    private DocumentFacade documentFacade;

    /**
     * Create new document
     *
     * @param document DTO representing document
     * @return last create document
     * @throws HTTPException
     * @throws Exception
     */
    @POST
    @Consumes({"application/xml"})
    public DocumentDTO create(DocumentDTO document) throws HTTPException, Exception {
        try {
            Document entity = new Document();
            entity.setCode(document.getCode());
            entity.setDate(new java.sql.Date(document.getDate().getTime()));
            entity.setName(document.getName());
            documentFacade.create(entity);
            entity = documentFacade.findLastInserted();
            return fillDTO(entity);
        } catch (Exception e) {
            document.setFailed(true);
            Logger.getLogger(DocumentFacadeREST.class.getName()).log(Level.SEVERE, LoggableBean.LOG_MESSAGE + "creating document with code: " + (document != null ? document.getCode() : null), e);
            return document;
        }
    }

    /**
     * Edit document
     *
     * @param document DTO representing document
     * @return edited document from database
     * @throws HTTPException
     */
    @PUT
    @Consumes({"application/xml"})
    public DocumentDTO edit(DocumentDTO document) throws HTTPException, Exception {
        try {
            Document entity = new Document();
            entity.setId(document.getId());
            entity.setCode(document.getCode());
            entity.setDate(new java.sql.Date(document.getDate().getTime()));
            entity.setName(document.getName());
            entity = documentFacade.edit(entity);
            return fillDTO(entity);
        } catch (Exception e) {
            Logger.getLogger(DocumentFacadeREST.class.getName()).log(Level.SEVERE, LoggableBean.LOG_MESSAGE + "editing document with id: " + (document != null ? document.getId() : null), e);
            document.setFailed(true);
            return document;
        }
    }

    /**
     * Delete document
     *
     * @param id id of selected document
     * @throws HTTPException
     */
    @DELETE
    @Path("/{id}")
    public void remove(@PathParam("id") Long id) throws HTTPException {
        try {
            if (id != null) {
                documentFacade.deleteItemsByIdDocument(id);
                Document entity = new Document();
                entity.setId(id);
                documentFacade.remove(entity);
            }
        } catch (Exception e) {
            Logger.getLogger(DocumentFacadeREST.class.getName()).log(Level.SEVERE, LoggableBean.LOG_MESSAGE + "deleting document with id: " + id, e);
            throw e;
        }
    }

    /**
     * Finding all documents in database
     *
     * @return DocumentListDTO entity with a list of all documents in database
     * @throws HTTPException
     */
    @GET
    @Produces({"application/xml"})
    @Path("/findAll")
    public DocumentListDTO findAll() throws HTTPException {
        List<DocumentDTO> documents = new ArrayList<DocumentDTO>();
        DocumentListDTO dtos = new DocumentListDTO();
        try {
            List<Document> docs = documentFacade.findAll();
            for (Document d : docs) {
                DocumentDTO dto = fillDTO(d);
                documents.add(dto);
            }
        } catch (Exception e) {
            Logger.getLogger(DocumentFacadeREST.class.getName()).log(Level.SEVERE, LoggableBean.LOG_MESSAGE + "reading all documents.", e);
            dtos.setFailed(true);
        }
        dtos.setDocuments(documents);
        return dtos;
    }

    /**
     * Finding last created document in database
     *
     * @param id id of wanted document
     * @return DocumentDTO entity representing document
     * @throws HTTPException
     */
    @GET
    @Produces({"application/xml"})
    @Path("/findDoc/{id}")
    public DocumentDTO findDocument(@PathParam("id") Long id) {
        DocumentDTO dto = new DocumentDTO();
        if (id != null) {
            try {
                Document d = documentFacade.find(id);
                dto = fillDTO(d);
            } catch (Exception e) {
                Logger.getLogger(DocumentFacadeREST.class.getName()).log(Level.SEVERE, LoggableBean.LOG_MESSAGE + "reading document with id: " + id, e);
                dto.setFailed(true);
            }
        }
        return dto;
    }

    /**
     * Finding last created document in database
     *
     * @return DocumentDTO entity representing document
     * @throws HTTPException
     */
    @GET
    @Produces({"application/xml"})
    @Path("/findLastDoc")
    public DocumentDTO findLastDocument() {
        DocumentDTO dto = new DocumentDTO();
        try {
            Document d = documentFacade.findLastInserted();
            dto = fillDTO(d);
        } catch (Exception e) {
            Logger.getLogger(DocumentFacadeREST.class.getName()).log(Level.SEVERE, LoggableBean.LOG_MESSAGE + "reading last inserted document: ", e);
            dto.setFailed(true);
        }
        return dto;
    }

    private DocumentDTO fillDTO(Document d) throws Exception {
        DocumentDTO dto = new DocumentDTO();
        dto.setId(d.getId());
        dto.setCode(d.getCode());
        dto.setName(d.getName());
        dto.setDate(new java.util.Date(d.getDate().getTime()));
        if (d.getItems() != null) {
            for (DocumentItem item : d.getItems()) {
                DocumentItemDTO itemDto = new DocumentItemDTO();
                itemDto.setId(item.getId());
                itemDto.setName(item.getName());
                itemDto.setPrice(item.getPrice());
                itemDto.setId_document(d.getId());
                itemDto.setDocCode(d.getCode());
                if (dto.getItems() == null) {
                    dto.setItems(new ArrayList<DocumentItemDTO>());
                }
                dto.getItems().add(itemDto);
            }
        }
        return dto;
    }
}

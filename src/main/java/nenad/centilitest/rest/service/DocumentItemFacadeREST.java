/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nenad.centilitest.rest.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.xml.ws.http.HTTPException;
import nenad.centilitest.common.LoggableBean;
import nenad.centilitest.dto.DocumentItemDTO;
import nenad.centilitest.dto.DocumentItemListDTO;
import nenad.centilitest.entity.Document;
import nenad.centilitest.entity.DocumentItem;
import nenad.centilitest.sessions.DocumentFacade;
import nenad.centilitest.sessions.DocumentItemFacade;

/**
 * REST Service for exposing DocumentItem entities via DTO
 *
 * @author nenad <barugdzicnenad@gmail.com>
 */
@Stateless
@Path("/documentItems")
public class DocumentItemFacadeREST implements Serializable {

    @EJB
    private DocumentItemFacade itemfacade;
    @EJB
    private DocumentFacade documentfacade;

    /**
     * Create new item
     *
     * @param itemDTO DTO representing item
     * @throws HTTPException
     */
    @POST
    @Consumes({"application/xml"})
    public DocumentItemDTO create(DocumentItemDTO itemDTO) throws HTTPException, Exception {
        try {
            DocumentItem entity = new DocumentItem();
            entity.setName(itemDTO.getName());
            entity.setPrice(itemDTO.getPrice());
            Document document = documentfacade.find(itemDTO.getId_document());
            entity.setDocument(document);
            itemfacade.create(entity);
            entity = itemfacade.findLastInserted();
            return fillDTO(entity);
        } catch (Exception e) {
            Logger.getLogger(DocumentFacadeREST.class.getName()).log(Level.SEVERE, LoggableBean.LOG_MESSAGE + "adding new item: ", e);
            itemDTO.setFailed(true);
            return itemDTO;
        }
    }

    /**
     * Edit item
     *
     * @param itemDTO DTO representing item
     * @return edited item from database
     * @throws HTTPException
     */
    @PUT
    @Consumes({"application/xml"})
    public DocumentItemDTO edit(DocumentItemDTO itemDTO) throws HTTPException, Exception {
        try {
                DocumentItem entity = new DocumentItem();
                entity.setId(itemDTO.getId());
                entity.setName(itemDTO.getName());
                entity.setPrice(itemDTO.getPrice());
                Document document = documentfacade.find(itemDTO.getId_document());
                entity.setDocument(document);
                entity = itemfacade.edit(entity);
                return fillDTO(entity);
        } catch (Exception e) {
            Logger.getLogger(DocumentFacadeREST.class.getName()).log(Level.SEVERE, LoggableBean.LOG_MESSAGE + "editing item with id: " + (itemDTO != null ? itemDTO.getId() : null), e);
            itemDTO.setFailed(true);
            return itemDTO;
        }
    }

    /**
     * Delete item
     *
     * @param id id of selected item
     * @throws HTTPException
     */
    @DELETE
    @Path("/{id}")
    public void remove(@PathParam("id") Long id) throws HTTPException {
        try {
            if (id != null) {
                itemfacade.remove(itemfacade.find(id));
            }
        } catch (Exception e) {
            Logger.getLogger(DocumentFacadeREST.class.getName()).log(Level.SEVERE, LoggableBean.LOG_MESSAGE + "deleting item with id: " + id, e);
            throw e;
        }
    }

    /**
     * Finding all items for selected document in database
     *
     * @param id_document id of selected document
     * @return DocumentItemListDTO entity with a list of items
     * @throws HTTPException
     */
    @GET
    @Path("/findByIdDocument/{id_document}")
    @Produces({"application/xml"})
    public DocumentItemListDTO findAllDocumentItemsByDocument(@PathParam("id_document") Long id_document) {
        List<DocumentItemDTO> items = new ArrayList<DocumentItemDTO>();
        DocumentItemListDTO itemsDTO = new DocumentItemListDTO();
        try {
            if (id_document != null) {
                List<DocumentItem> docItems = itemfacade.findAllOfDocument(id_document);
                for (DocumentItem item : docItems) {
                    DocumentItemDTO dto = fillDTO(item);
                    items.add(dto);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(DocumentFacadeREST.class.getName()).log(Level.SEVERE, LoggableBean.LOG_MESSAGE + "reading all items for document with id_document: " + id_document, e);
            itemsDTO.setFailed(true);
        }
        itemsDTO.setItems(items);
        return itemsDTO;
    }

    /**
     * Return item with selected id from database
     *
     * @param id id of selected item
     * @return DocumentItemDTO DTO representing item
     * @throws HTTPException
     */
    @GET
    @Produces({"application/xml"})
    @Path("/findItem/{id}")
    public DocumentItemDTO findItem(@PathParam("id") Long id) {
        DocumentItemDTO dto = new DocumentItemDTO();
        try {
            if (id != null) {
                DocumentItem item = itemfacade.find(id);
                dto = fillDTO(item);
            }
        } catch (Exception e) {
            Logger.getLogger(DocumentFacadeREST.class.getName()).log(Level.SEVERE, LoggableBean.LOG_MESSAGE + "reading item with id: " + id, e);
            dto.setFailed(true);
        }
        return dto;
    }

    /**
     * Return last created item from database
     *
     * @return DocumentItemDTO DTO representing item
     * @throws HTTPException
     */

    /**
     * Finding all items in database
     *
     * @return DocumentItemListDTO entity with a list of items
     * @throws HTTPException
     */
    @GET
    @Path("/findAll")
    @Produces({"application/xml"})
    public DocumentItemListDTO findAll() {
        List<DocumentItemDTO> items = new ArrayList<DocumentItemDTO>();
        DocumentItemListDTO itemsDTO = new DocumentItemListDTO();
        try {
            List<DocumentItem> docItems = itemfacade.findAll();
            for (DocumentItem item : docItems) {
                DocumentItemDTO dto = fillDTO(item);
                items.add(dto);
            }
        } catch (Exception e) {
            Logger.getLogger(DocumentFacadeREST.class.getName()).log(Level.SEVERE, LoggableBean.LOG_MESSAGE + "reading all items: ", e);
            itemsDTO.setFailed(true);
        }
        itemsDTO.setItems(items);
        return itemsDTO;
    }

    private DocumentItemDTO fillDTO(DocumentItem item) throws Exception {
        DocumentItemDTO dto = new DocumentItemDTO();
        dto.setId(item.getId());
        dto.setName(item.getName());
        dto.setPrice(item.getPrice());
        dto.setId_document(item.getDocument().getId());
        dto.setDocCode(item.getDocument().getCode());
        return dto;
    }
}

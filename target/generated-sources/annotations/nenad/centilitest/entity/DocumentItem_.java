package nenad.centilitest.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DocumentItem.class)
public abstract class DocumentItem_ {

	public static volatile SingularAttribute<DocumentItem, Double> price;
	public static volatile SingularAttribute<DocumentItem, Document> document;
	public static volatile SingularAttribute<DocumentItem, String> name;
	public static volatile SingularAttribute<DocumentItem, Long> id;

}


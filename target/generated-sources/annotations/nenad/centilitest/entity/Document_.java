package nenad.centilitest.entity;

import java.sql.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Document.class)
public abstract class Document_ {

	public static volatile SingularAttribute<Document, Date> date;
	public static volatile SingularAttribute<Document, String> code;
	public static volatile SingularAttribute<Document, String> name;
	public static volatile SingularAttribute<Document, Long> id;
	public static volatile ListAttribute<Document, DocumentItem> items;

}

